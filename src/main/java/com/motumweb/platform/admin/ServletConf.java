package com.motumweb.platform.admin;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.motumweb.platform.dry.config.ServletConfigFactory;

@Configuration
public class ServletConf {
	
	@Value("${SERVICE_SUBSCRIPTIONS_ENV}")
	private String serviceSubscriptionsEnv;

	@Bean
	public TomcatServletWebServerFactory getServlet() {
		TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
		factory.setPort(ServletConfigFactory.getPort(serviceSubscriptionsEnv));
		return factory;
	}
}
