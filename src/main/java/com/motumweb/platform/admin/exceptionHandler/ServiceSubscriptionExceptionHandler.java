package com.motumweb.platform.admin.exceptionHandler;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.motumweb.platform.servicesubscriptions.dtos.Response;
import com.motumweb.platform.servicesubscriptions.exceptions.NotFoundException;
import com.motumweb.platform.admin.controllers.AgreementApi;
import com.motumweb.platform.admin.controllers.ServiceApi;

@ControllerAdvice(assignableTypes = {
		ServiceApi.class,
		AgreementApi.class
})
public class ServiceSubscriptionExceptionHandler {
	private static final Logger LOG = LoggerFactory
			.getLogger(MethodHandles.lookup().lookupClass());

	@ExceptionHandler(NotFoundException.class)
    ResponseEntity<?> handleNotFoundException(NotFoundException e) {
		Response response = new Response();        
        response.setCode(e.getCode());
        response.setMessage(e.getMessage());        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }
}
