package com.motumweb.platform.admin.configuration;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerDocumentationConfig {
	
	@Bean
	public Docket customImplementation() {
		return new Docket(DocumentationType.SWAGGER_2)
			.select()
			.apis(RequestHandlerSelectors.basePackage("com.motumweb.platform.admin.controllers"))
			.build()
			.useDefaultResponseMessages(false)
			.directModelSubstitute(org.threeten.bp.LocalDate.class, java.sql.Date.class)
			.directModelSubstitute(org.threeten.bp.OffsetDateTime.class, java.util.Date.class)
			.apiInfo(apiInfo())
			.securitySchemes(this.securitySchemes())
			.securityContexts(this.securityContexts());
	}

	ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Complementary api").description("Complementary services").termsOfServiceUrl("")
				.version("0.0.1").contact(new Contact("", "", "")).build();
	}

	private List<? extends SecurityScheme> securitySchemes() {
		return Arrays.asList(new ApiKey("api_key", "key", "query")
			, new ApiKey("JWT", "Authorization", "header"));
	}

	private List<SecurityContext> securityContexts() {
	        return Arrays.asList(
	                SecurityContext.builder()
	                    .forPaths(Predicates.not(PathSelectors.regex("/liveness")))
	                    .securityReferences(securityReferences()).build());
	}
	
	private List<SecurityReference> securityReferences() {
		return Arrays.asList(SecurityReference.builder().reference("api_key").scopes(new AuthorizationScope[0]).build(),
			SecurityReference.builder().reference("JWT").scopes(new AuthorizationScope[0]).build());
	}

}
