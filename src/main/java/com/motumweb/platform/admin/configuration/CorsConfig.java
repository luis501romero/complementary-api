package com.motumweb.platform.admin.configuration;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CorsConfig {
	
	 @Bean
		public FilterRegistrationBean<ApiOriginFilter> corsFilterRegistration() {
			FilterRegistrationBean<ApiOriginFilter> registrationBean = new FilterRegistrationBean<ApiOriginFilter>(new ApiOriginFilter());
			registrationBean.setName("CORS Filter");
			registrationBean.addUrlPatterns("/*");
			registrationBean.setOrder(1);
			return registrationBean;
		}


}
