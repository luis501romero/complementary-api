package com.motumweb.platform.admin.controllers.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.motumweb.platform.admin.controllers.ServiceApi;
import com.motumweb.platform.servicesubscriptions.dtos.ServiceDTO;
import com.motumweb.platform.servicesubscriptions.exceptions.NotFoundException;
import com.motumweb.platform.servicesubscriptions.services.ServiceServices;

@Component
public class ServiceApiController implements ServiceApi {
	
	private ServiceServices serviceServices;
	
	public ServiceApiController(ServiceServices serviceServices) {
		this.serviceServices = serviceServices;
	}

	@Override
	public ResponseEntity<ServiceDTO> getServiceByIdentifier(Integer clientId, Integer userId, Integer serviceId) throws NotFoundException{
		return ResponseEntity.ok(this.serviceServices.getAgreementByServiceIdentifier(clientId, userId, serviceId));
	}

}
