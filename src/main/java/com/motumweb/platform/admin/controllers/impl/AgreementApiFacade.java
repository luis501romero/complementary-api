package com.motumweb.platform.admin.controllers.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.motumweb.platform.admin.controllers.AgreementApi;
import com.motumweb.platform.servicesubscriptions.dtos.AgreementDTO;
import com.motumweb.platform.servicesubscriptions.dtos.Response;
import com.motumweb.platform.servicesubscriptions.exceptions.NotFoundException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
@Api(value = "Agreements")
public class AgreementApiFacade implements AgreementApi {
	
	private AgreementApiController agreementApiController;
	
	public AgreementApiFacade(AgreementApiController agreementApiController) {
		this.agreementApiController = agreementApiController;
	}

	@Override
	@ApiOperation(value = "Save an agreement.", 
		nickname = "saveAgreement",
		notes = "Save the agreement",
		response = Response.class,
		tags = { "Agreements" })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = Response.class)
	})
	@RequestMapping(value = "/clients/{clientId}/agreements", method = RequestMethod.POST)
	public ResponseEntity<Response> saveAgreement(
			@ApiParam(value = "Client identifier", required = true) @PathVariable("clientId") Integer clientId, 
			@ApiParam(value = "User Identifier", required = true) @RequestHeader("tm-user")  Integer userId, 
			@ApiParam(value = "Agreement", required = true) @RequestBody AgreementDTO agreement) throws NotFoundException {
		return agreementApiController.saveAgreement(clientId, userId, agreement);
	}

}
