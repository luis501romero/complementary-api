package com.motumweb.platform.admin.controllers;

import org.springframework.http.ResponseEntity;

import com.motumweb.platform.servicesubscriptions.dtos.ServiceDTO;
import com.motumweb.platform.servicesubscriptions.exceptions.NotFoundException;

public interface ServiceApi {

	ResponseEntity<ServiceDTO> getServiceByIdentifier(Integer clientId, Integer userId, Integer serviceId) throws NotFoundException;
	
}
