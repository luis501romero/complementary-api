package com.motumweb.platform.admin.controllers;

import org.springframework.http.ResponseEntity;

import com.motumweb.platform.servicesubscriptions.dtos.AgreementDTO;
import com.motumweb.platform.servicesubscriptions.dtos.Response;
import com.motumweb.platform.servicesubscriptions.exceptions.NotFoundException;

public interface AgreementApi {
	
	public ResponseEntity<Response> saveAgreement(Integer clientId, Integer userId, AgreementDTO agreement) throws NotFoundException;

}
