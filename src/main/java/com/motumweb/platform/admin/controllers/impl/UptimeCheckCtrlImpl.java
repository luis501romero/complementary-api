package com.motumweb.platform.admin.controllers.impl;

import com.motumweb.platform.admin.controllers.UptimeCheckCtrl;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@Controller
@Api(tags= {"liveness"}, description = "Uptime Check Ctrl Impl")
public class UptimeCheckCtrlImpl implements UptimeCheckCtrl {
	
	@ApiOperation(value="To check if the API is alive")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success")})
	@Override
	@GetMapping("/liveness")
	public ResponseEntity<?> isAlive() {
		return ResponseEntity.ok().build();
	}

}
