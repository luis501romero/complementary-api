package com.motumweb.platform.admin.controllers.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.motumweb.platform.admin.controllers.AgreementApi;
import com.motumweb.platform.servicesubscriptions.dtos.AgreementDTO;
import com.motumweb.platform.servicesubscriptions.dtos.Response;
import com.motumweb.platform.servicesubscriptions.exceptions.NotFoundException;
import com.motumweb.platform.servicesubscriptions.services.AgreementServices;

@Component
public class AgreementApiController implements AgreementApi {
	
	private AgreementServices agreementServices;
	
	public AgreementApiController(AgreementServices agreementServices) {
		this.agreementServices = agreementServices;
	}

	@Override
	public ResponseEntity<Response> saveAgreement(Integer clientId, Integer userId, AgreementDTO agreement) throws NotFoundException {
		this.agreementServices.saveAgreement(clientId, userId, agreement);
		Response res = new Response();
		res.setCode("Ok");
		res.setMessage("Agreement saved correctly");
		return ResponseEntity.ok(res);
	}
	
	

}
