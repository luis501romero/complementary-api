package com.motumweb.platform.admin.controllers.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.motumweb.platform.admin.controllers.ServiceApi;
import com.motumweb.platform.servicesubscriptions.dtos.ServiceDTO;
import com.motumweb.platform.servicesubscriptions.exceptions.NotFoundException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
@Api(value = "Services")
public class ServiceApiFacade implements ServiceApi{
	
	private ServiceApiController serviceApiController;
	
	public ServiceApiFacade(ServiceApiController serviceApiController) {
		this.serviceApiController = serviceApiController;
	}

	@Override
	@ApiOperation(value = "Service by identifier.", 
		nickname = "getService",
		notes = "Get the service by the service identifier",
		response = ServiceDTO.class,
		tags = {"Services", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", response = ServiceDTO.class)
	})
	@RequestMapping(value = "/clients/{clientId}/services/{serviceId}", method = RequestMethod.GET)
	public ResponseEntity<ServiceDTO> getServiceByIdentifier(
			@ApiParam(value = "Client Identifier", required = true) @PathVariable("clientId") Integer clientId, 
			@ApiParam(value = "User Identifier", required = true) @RequestHeader("tm-user") Integer userId, 
			@ApiParam(value = "Service Identifier", required = true) @PathVariable("serviceId") Integer serviceId) throws NotFoundException {
		return serviceApiController.getServiceByIdentifier(clientId, userId, serviceId);
	}

}
