package com.motumweb.platform.admin.controllers;

import org.springframework.http.ResponseEntity;

public interface UptimeCheckCtrl {
	ResponseEntity<?> isAlive();
}
